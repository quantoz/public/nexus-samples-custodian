﻿using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic.Customers;
using CustodianSampleApi.Data.DTOs.Repo.Customers;

namespace CustodianSampleApi.Repo
{
    public interface ICustomerRepo
    {
        public Task<ConfirmCustomerDto> Create(CreateCustomerDto dto);
        public Task<ConfirmCustomerDto> Get(int customerId);
        public Task<ConfirmCustomerDto> Get(string address);
    }
}
