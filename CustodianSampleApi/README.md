# Proxy API Gateway

## Configuration

The `appsettings.json` file contains the settings and client credentials that you need to run the app, contact Quantoz Nexus support to obtain the needed configuration to connect to your Nexus Tenant.

## Installation

- Install .NET Core 3.1 [Download link](https://dotnet.microsoft.com/download). Download and install the SDK as per your operating system. We always recommend to use LTS version.
- Install a .NET Core IDE 
    - Visual Studio 2019 for Windows
        - Visual Studio Community has a special license that is free to use with some requirements.
    - Visual Studio Code for Linux, Mac and Windows
        - To run the project with Visual Studio Code you need to install C# extension.
    - JetBrains Rider for Linux, Mac and Windows
- The app requires a local in-memory database to store user credentials. For that you need to install EF core. To do that execute `dotnet tool install --global dotnet-ef --version 3.1.11` on your terminal or command prompt. You can verify the installation by executing `dotnet ef` command.
- After the successful installation of EF core you can create the database by the command `dotnet ef database update -c ApplicationDbContext`.
- Execute `dotnet run` to run the project.

## OpenAPI specification

The API exposes a generated OpenAPI spec (version 3).
This specification is generated based upon the source code and there's also an integrated Swagger UI to show the different endpoints and experiment with them.
The URL of Swagger UI is `https://localhost:{port}/swagger/index.html`.
