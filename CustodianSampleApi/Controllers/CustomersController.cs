﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CustodianSampleApi.Data.DTOs.Repo.Customers;
using CustodianSampleApi.Logic.Interfaces;
using static IdentityServer4.IdentityServerConstants;
using CustodianSampleApi.Data.DTOs.Controller.Customer;
using CustodianSampleApi.Data.DTOs.Logic.Customers;
using Microsoft.Extensions.Configuration;
using System.Linq;
using CustodianSampleApi.Data.DTOs.Logic.Customer;
using System.Collections.Generic;

namespace CustodianSampleApi.Controllers
{
    [Authorize(LocalApi.PolicyName)]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : BaseController
    {
        private readonly ICustomerLogic _customerLogic;

        private readonly string _acceptedAnonymousRequestToken;

        public CustomersController(
            IConfiguration configuration,
            ICustomerLogic customerLogic)
            : base()
        {
            _acceptedAnonymousRequestToken = configuration.GetValue<string>("accepted-anonymous-request-token");
            _customerLogic = customerLogic;
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GetCustomerDto>> GetCustomer([FromRoute] int id)
        {
            var customer = await _customerLogic.Get(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(new GetCustomerDto { Email = customer.Email });
        }

        // POST:
        [AllowAnonymousAttribute]
        [HttpPost]
        public async Task<ActionResult<ConfirmCustomerDto>> CreateCustomer([FromBody] CreateCustomerDto dto)
        {
            if (dto.AnonymousRequestToken != _acceptedAnonymousRequestToken)
            {
                return BadRequest("Incorrect Anonymous Request Token");
            }

            if (string.IsNullOrWhiteSpace(dto.Password))
            {
                return BadRequest("Password is not filled in");
            }

            try
            {
                return CreatedAtAction("CreateCustomer", await _customerLogic.Create(dto));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (HttpRequestException e)
            {
                return Problem(e.Message);
            }
        }

        [HttpGet("{customerCode}/accounts")]
        public async Task<ActionResult<IEnumerable<AccountDTO>>> GetCustomerAccount([FromRoute] string customerCode)
        {
            if (customerCode == null)
            {
                return NotFound();
            }

            var result = await _customerLogic.GetCustomerAccounts(customerCode);

            if (result != null)
            {
                return Ok(result.Records);
            }
            else
            {
                return BadRequest("Customer accounts not found");
            }
        }
    }
}
