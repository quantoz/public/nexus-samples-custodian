﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CustodianSampleApi.Logic.Interfaces;
using static IdentityServer4.IdentityServerConstants;
using Microsoft.Extensions.Configuration;
using CustodianSampleApi.Data.DTOs.Logic;

namespace CustodianSampleApi.Controllers
{

    [Authorize(LocalApi.PolicyName)]
    [Route("api/[controller]")]
    [ApiController]
    public class PriceController : BaseController
    {
        private readonly string _acceptedAnonymousRequestToken;
        private readonly IPriceLogic _priceLogic;

        public PriceController(
            IConfiguration configuration,
            IPriceLogic priceLogic)
            : base()
        {
            _acceptedAnonymousRequestToken = configuration.GetValue<string>("accepted-anonymous-request-token");
            _priceLogic = priceLogic;
        }

        [HttpGet("{currencyCode}")]
        public async Task<ActionResult<GetPrices>> GetBalances([FromRoute] string currencyCode)
        {
            var result = await _priceLogic.GetPrice(currencyCode);

            if (result == null)
            {
                return BadRequest($"Prices is not found");
            }

            return Ok(result);
        }

        [HttpGet("{currencyCode}/{cryptoCode}")]
        public async Task<ActionResult<GetPrices>> GetCryptoPrices([FromRoute] string currencyCode, [FromRoute] string cryptoCode)
        {
            var result = await _priceLogic.GetPrice(currencyCode, cryptoCode);

            if (result == null)
            {
                return BadRequest($"Prices is not found");
            }

            return Ok(result);
        }
    }
}