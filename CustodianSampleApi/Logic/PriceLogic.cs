﻿using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;
using CustodianSampleApi.Logic.Interfaces;
using CustodianSampleApi.Services;

namespace CustodianSampleApi.Logic
{
    public class PriceLogic : IPriceLogic
    {
        private readonly INexusApiService _nexusApiService;

        public PriceLogic(INexusApiService nexusApiService)
        {
            _nexusApiService = nexusApiService;
        }
        public async Task<GetPrices> GetPrice(string currencyCode)
        {
            return await _nexusApiService.GetPrice(currencyCode);
        }
        public async Task<GetPrices> GetPrice(string currencyCode, string cryptoCode)
        {
            return await _nexusApiService.GetPrice(currencyCode, cryptoCode);
        }
    }
}
