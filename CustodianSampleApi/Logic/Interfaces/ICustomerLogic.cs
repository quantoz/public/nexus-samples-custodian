﻿using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic.Customer;
using CustodianSampleApi.Data.DTOs.Logic.Customers;
using CustodianSampleApi.Data.DTOs.Repo.Customers;

namespace CustodianSampleApi.Logic.Interfaces
{
    public interface ICustomerLogic
    {
        public Task<ConfirmCustomerDto> Create(CreateCustomerDto dto);
        public Task<ConfirmCustomerDto> Get(int deviceId);
        public Task<CustomerAccountsDTO> GetCustomerAccounts(string customerCode);
    }
}
