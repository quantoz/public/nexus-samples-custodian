﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic.Transaction;
using CustodianSampleApi.Logic.Interfaces;
using CustodianSampleApi.Services;

namespace CustodianSampleApi.Logic
{
    public class TransactionLogic : ITransactionLogic
    {
        private readonly INexusApiService _nexusApiService;

        public TransactionLogic(
            INexusApiService nexusApiService)
        {
            _nexusApiService = nexusApiService;
        }

        public async Task<List<GetTransactionDTO>> GetTransactions(string cryptoCode, string accountCode, string status, string currencyCode, string customerCode)
        {
            return await _nexusApiService.GetTransactions(cryptoCode, accountCode, status, currencyCode, customerCode);
        }

        public async Task<TotalTransactionReponseDTO> CreateBuyTransaction(BuyTransactionDTO buyTransaction)
        {
            return await _nexusApiService.CreateBuyTransaction(buyTransaction);
        }

        public async Task<TotalTransactionReponseDTO> CreateSellTransaction(SellTransactionDTO sellTransaction)
        {
            return await _nexusApiService.CreateSellTransaction(sellTransaction);
        }

        public async Task<CustodianSwapResponseDTO> CreateSwapTransaction(SwapTransactionDTO swapTransaction)
        {
            return await _nexusApiService.CreateSwapTransaction(swapTransaction);
        }

        public async Task<TotalTransactionReponseDTO> SimulateBuyTransaction(BuyTransactionDTO buyTransaction)
        {
            return await _nexusApiService.SimulateBuyTransaction(buyTransaction);
        }

        public async Task<TotalTransactionReponseDTO> SimulateSellTransaction(SellTransactionDTO sellTransaction)
        {
            return await _nexusApiService.SimulateSellTransaction(sellTransaction);
        }
    }
}
