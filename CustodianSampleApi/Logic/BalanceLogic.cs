﻿using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;
using CustodianSampleApi.Logic.Interfaces;
using CustodianSampleApi.Services;

namespace CustodianSampleApi.Logic
{
    public class BalanceLogic : IBalanceLogic
    {
        private readonly INexusApiService _nexusApiService;

        public BalanceLogic(INexusApiService nexusApiService)
        {
            _nexusApiService = nexusApiService;
        }
        public async Task<BalanceDTO> GetBalance(string customerCode)
        {
            return await _nexusApiService.GetBalance(customerCode);
        }
    }
}
