﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;
using CustodianSampleApi.Data.DTOs.Logic.Customer;
using CustodianSampleApi.Data.DTOs.Logic.Transaction;
using CustodianSampleApi.Data.DTOs.Services.NexusApiService.Account;
using CustodianSampleApi.Data.DTOs.Services.NexusApiService.Customer;

namespace CustodianSampleApi.Services
{
    public interface INexusApiService
    {
        Task<GetCustomerResponse> GetCustomer(string customerCode);
        Task<PostCustomerResponse> CreateCustomer(string customerCode, string email);
        Task<GetAccountResponse> GetAccount(string accountCode);
        Task<PostAccountResponse> CreateAccount(string customerCode);
        Task<BalanceDTO> GetBalance(string customerCode);
        Task<CustomerAccountsDTO> GetCustomerAccounts(string customerCode);
        Task<GetPrices> GetPrice(string currencyCode);
        Task<List<GetTransactionDTO>> GetTransactions(string cryptoCode, string accountCode, string status, string currencyCode, string customerCode);
        Task<TotalTransactionReponseDTO> CreateBuyTransaction(BuyTransactionDTO buyTransaction);
        Task<GetPrices> GetPrice(string currencyCode, string cryptoCode);
        Task<TotalTransactionReponseDTO> CreateSellTransaction(SellTransactionDTO sellTransaction);
        Task<CustodianSwapResponseDTO> CreateSwapTransaction(SwapTransactionDTO swapTransaction);
        Task<TotalTransactionReponseDTO> SimulateBuyTransaction(BuyTransactionDTO buyTransaction);
        Task<TotalTransactionReponseDTO> SimulateSellTransaction(SellTransactionDTO sellTransaction);
    }
}
