﻿using System.ComponentModel.DataAnnotations;

namespace CustodianSampleApi.Data.DTOs.Repo.Customers
{
    public class CreateCustomerDto
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9]+(\.?[0-9a-zA-Z_-])*@[a-zA-Z0-9]+[0-9a-zA-Z_-]*.[a-zA-Z]+$")]
        public string Email { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 8)]
        public string Password { get; set; }

        [Required]
        public string AnonymousRequestToken { get; set; }
    }
}
