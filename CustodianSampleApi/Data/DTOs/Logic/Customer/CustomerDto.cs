﻿using System.ComponentModel.DataAnnotations;

namespace CustodianSampleApi.Data.DTOs.Logic.Customers
{
    public class ConfirmCustomerDto
    {
        public string Email { get; set; }
        public string CryptoAddress { get; set; }
    }
}
