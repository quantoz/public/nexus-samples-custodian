﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustodianSampleApi.Data.DTOs.Logic.Customer
{
    public class CustomerAccountsDTO
    {
        public IEnumerable<AccountDTO> Records;
    }
    public class AccountDTO
    {
        public string AccountCode { get; set; }

        /// <summary>
        /// Customer unique identifier.
        /// </summary>
        public string CustomerCode { get; set; }

        /// <summary>
        /// Timestamp of creation of the Account
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Timestamp of activation of the Account
        /// </summary>
        public DateTime? Activated { get; set; }

        /// <summary>
        /// Address in Nexus to receive cryptocurrency.
        /// </summary>
        public string DCReceiveAddress { get; set; }

        /// <summary>
        /// Address of the Customer to send cryptocurrency.
        ///
        /// OR
        ///
        /// User supplied Address of the Customer to send and receive Tokens.
        /// </summary>
        public string CustomerCryptoAddress { get; set; }

        /// <summary>
        /// Cryptocurrency identifier.
        /// </summary>
        public string DCCode { get; set; }

        /// <summary>
        /// Status of the account.
        ///
        /// NEW | INVALID | VALID | ACTIVE | DELETED
        /// </summary>
        /// <example>
        /// NEW | INVALID | VALID | ACTIVE | DELETED
        /// </example>
        public string AccountStatus { get; set; }

        /// <summary>
        /// Type of the account.
        ///
        /// CUSTODIAN | BROKER | TOKEN
        /// </summary>
        /// <example>
        /// CUSTODIAN | BROKER | TOKEN
        /// </example>
        public string AccountType { get; set; }
    }
}
