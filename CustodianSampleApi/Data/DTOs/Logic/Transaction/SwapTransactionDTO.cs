﻿namespace CustodianSampleApi.Data.DTOs.Logic.Transaction
{
    public class SwapTransactionDTO
    {
        public string PaymentMethodCode { get; set; }

        public string CustomerCode { get; set; }

        public SwapSourceDTO Source { get; set; }

        public SwapDestinationDTO Destination { get; set; }
    }

    public class SwapSourceDTO
    {
        public string AccountCode { get; set; }

        public string CryptoCode { get; set; }

        public decimal CryptoAmount { get; set; }

        public decimal? RequestedPrice { get; set; }
    }

    public class SwapDestinationDTO
    {
        public string AccountCode { get; set; }

        public string CryptoCode { get; set; }

        public decimal? RequestedPrice { get; set; }
    }
}
