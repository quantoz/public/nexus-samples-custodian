﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustodianSampleApi.Data.DTOs.Services.NexusApiService
{
    public class CustomResultHolder<T>
    {
        public string Message { get; }
        public IEnumerable<string> Errors { get; }

        public T Values { get; set; }

        public CustomResultHolder(T values, IEnumerable<string> errors, string message)
        {
            Values = values;
            Errors = errors;
            Message = message;
        }
    }
}
