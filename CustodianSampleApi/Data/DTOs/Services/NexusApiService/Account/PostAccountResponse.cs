﻿using System;

namespace CustodianSampleApi.Data.DTOs.Services.NexusApiService.Account
{
    public class PostAccountResponse
    {
        public string Guid { get; set; }
        public string AccountCode { get; set; }
        public string CustomerCode { get; set; }
        public DateTime Created { get; set; }
        public string DCReceiveAddress { get; set; }
        public string CustomerCryptoAddress { get; set; }
        public string DCCode { get; set; }
        public string AccountStatus { get; set; }
    }
}
