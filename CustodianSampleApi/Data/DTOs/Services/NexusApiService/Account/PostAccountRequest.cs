﻿namespace CustodianSampleApi.Data.DTOs.Services.NexusApiService.Account
{
    public class PostAccountRequest
    {
        public string AccountType { get; set; } = "CUSTODIAN";
        public string CryptoCode { get; set; }
    }
    public class CustodianSettingsRequest
    {
        /// <summary>
        /// If set to true, Nexus will generate a receive address for this account
        /// For the demo app we will not support any on-chain transactions.
        /// But its good to have addresses to show that a customer can even save his own cryptos in his custodian accounts.
        /// </summary>
        public bool GenerateReceiveAddress { get; set; } = true;
    }
}
