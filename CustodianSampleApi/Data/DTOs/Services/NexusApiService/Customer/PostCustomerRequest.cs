﻿using System;
using CustodianSampleApi.Data.DTOs.Services.NexusApiService.Account;

namespace CustodianSampleApi.Data.DTOs.Services.NexusApiService.Customer
{
    public class PostCustomerRequest
    {
        public string CustomerCode { get; set; }
        public string CurrencyCode { get; set; }
        public string TrustLevel { get; set; }
        public string Email { get; set; }
        public PostAccountRequest[] Accounts { get; set; }

        public PostCustomerRequest(string customerCode, string email)
        {
            CustomerCode = customerCode;
            Email = email;
            CurrencyCode = "EUR";
            TrustLevel = "Active";
            Accounts = new PostAccountRequest[] {
                new PostAccountRequest
                {
                    CryptoCode = "ETH"
                },
                new PostAccountRequest
                {
                    CryptoCode = "BCH"
                },
                new PostAccountRequest
                {
                    CryptoCode = "BTC"
                },
                new PostAccountRequest
                {
                    CryptoCode = "LTC"
                },
                new PostAccountRequest
                {
                    CryptoCode = "XRP"
                },
                new PostAccountRequest
                {
                    CryptoCode = "XLM"
                }
            };
        }
    }
}
