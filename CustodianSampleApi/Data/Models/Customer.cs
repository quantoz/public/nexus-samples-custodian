﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CustodianSampleApi.Data.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        public DateTime Created { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string NexusAccountCode { get; set; }
    }
}
