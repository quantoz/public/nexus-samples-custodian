﻿using Newtonsoft.Json.Converters;

namespace CustodianSampleApi.Data
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public const string DateFormatA = "yyyy-MM-ddTHH:mm:ssZ";

        public CustomDateTimeConverter()
        {
            DateTimeFormat = DateFormatA;
        }
    }

}
