import React from "react";
import { Platform, StatusBar } from "react-native";
import { StyleProvider, Container } from "native-base";
import getTheme from "_nativeBaseTheme/components";
import platformTheme from "_nativeBaseTheme/variables/platform";
import AppNavigator from "_navigation/AppNavigator";

export default function App(props) {
  return (
    <StyleProvider style={getTheme(platformTheme)}>
      <Container>
        {Platform.OS === "ios" && <StatusBar barStyle="default" />}
        <AppNavigator />
      </Container>
    </StyleProvider>
  );
}
