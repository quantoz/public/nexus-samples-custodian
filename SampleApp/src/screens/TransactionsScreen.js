import React, { useState, useEffect } from "react";
import { SafeAreaView } from "react-native";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import TransactionsList from "_components/TransactionsList";
import {
  Content,
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right
} from "native-base";
import { Grid, Col } from "react-native-easy-grid";

export default function TransactionsScreen({ navigation }) {
  const [transactions, setTransactions] = useState(
    navigation.getParam("transactions", [])
  );
  const [counter, setCounter] = useState(1);

  useEffect(() => {
    let reload = true;

    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };

    var timer = setInterval(tick, 1000);

    return () => {
      reload = false;
      clearInterval(timer);
    };
  }, [counter]);

  const willFocusSubscription = navigation.addListener("willFocus", () => {
    setTransactions(navigation.getParam("transactions", []));
  });

  useEffect(() => {
    let reload = true;

    if (reload) {
      setTransactions(navigation.getParam("transactions", []));
    }

    return () => {
      reload = false;
      willFocusSubscription.remove();
    };
  }, [counter]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content padder>
          <LoadingSpinner area="tx-list-area">
            <Grid>
              <Col>
                <TransactionsList listItems={transactions} />
              </Col>
            </Grid>
          </LoadingSpinner>
        </Content>
      </Container>
    </SafeAreaView>
  );
}

TransactionsScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header darkTheme>
      <Left>
        <Button transparent onPress={() => navigation.goBack()}>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Title>Transactions</Title>
      </Body>
      <Right />
    </Header>
  )
});
