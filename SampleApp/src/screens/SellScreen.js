import React, { useState, useEffect } from "react";
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  Content,
  Text,
  View
} from "native-base";
import CurrencyWidget from "_components/CurrencyWidget";
import { SafeAreaView } from "react-native";
import PropTypes from "prop-types";
import RealTimeCryptoPriceWidget from "_components/RealTimeCryptoPriceWidget";
import FeesSummary from "_components/FeesSummary";
import { LoadingSpinner } from "_components/LoadingSpinner";
import {
  createSellTransaction,
  getSellTransactionFee,
  getPaymentMethodCode,
  getCryptoPrice
} from "../services/Server";

const SellScreen = props => {
  const [sellSourceAmount, setSellSourceAmount] = useState(0.0);
  const [cryptoPrice, setPrice] = useState({});
  const [totalFee, setTotalFee] = useState(0.0);
  const [feeSubTotalAmount, setFeeSubTotalAmount] = useState(0.0);
  const [cryptoAmount, setCryptoAmount] = useState(0.0);
  const [primaryButtonEnabled, setPrimaryButtonEnabled] = useState(false);
  const [user, setUser] = useState(props.navigation.getParam("user", {}));
  const [fiatBalance, setFiatBalance] = useState(
    props.navigation.getParam("fiatBalance", 0.0)
  );
  const [cryptoBalance, setCryptoBalance] = useState(
    props.navigation.getParam("balance", 0.0)
  );
  const [currencyCode, setCurrencyCode] = useState(
    props.navigation.getParam("currencyCode"),
    ""
  );
  const [pmCode, setPmCode] = useState(
    getPaymentMethodCode(currencyCode, "SELL")
  );
  const [customerCode, setCustomerCode] = useState(
    props.navigation.getParam("customerCode", "")
  );
  const [accountCode, setAccountCode] = useState(
    props.navigation.getParam("accountCode", "")
  );
  const [isLoading, setIsLoading] = useState(false);
  const [counter, setCounter] = useState(1);

  useEffect(() => {
    let reload = true;

    const fetchData = async () => {
      if (counter <= 1) {
        setIsLoading(true);
      }

      if (reload) {
        setPrice(await getCryptoPrice(user, currencyCode));
        if (isLoading) {
          setIsLoading(false);
        }
      }
    };

    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };

    var timer = setInterval(tick, 1000);

    fetchData();

    return () => {
      reload = false;
      clearInterval(timer);
    };
  }, [counter]);

  useEffect(() => {
    console.log("Sell screen ~ render ~ hook 2");
    let reload = true;

    const fetchData = async () => {
      if (reload) {
        if (sellSourceAmount > 0) {
          const finalFeeAndCryptoAmount = await getSellTransactionFee(user, {
            customerCode: customerCode,
            fiatValue: parseFloat(sellSourceAmount),
            cryptoCode: currencyCode,
            currencyCode: "EUR",
            paymentMethodCode: pmCode,
            accountCode: accountCode,
            requestedPrice: parseFloat(cryptoPrice.sell)
          });
          if (finalFeeAndCryptoAmount !== null) {
            setTotalFee(finalFeeAndCryptoAmount.fee);
            setCryptoAmount(finalFeeAndCryptoAmount.cryptoAmount);
          }
        } else {
          setTotalFee(0.0);
          setCryptoAmount(0.0);
        }
      }
    };
    fetchData();

    return () => {
      reload = false;
    };
  }, [sellSourceAmount, counter]);

  const handleSellSourceAmount = amount => {
    if (amount === "") {
      setSellSourceAmount(0.0);
    } else {
      setSellSourceAmount(amount);
    }

    const totalSellAmount = sellSourceAmount - totalFee;
    setFeeSubTotalAmount(totalSellAmount);

    if (
      amount === 0 ||
      amount === "" ||
      cryptoBalance === 0 ||
      amount > cryptoBalance
    ) {
      setPrimaryButtonEnabled(false);
    } else {
      setPrimaryButtonEnabled(true);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content padder>
          <View>
            <CurrencyWidget
              currencyName={props.navigation.getParam("currencyName")}
              currencyCode={currencyCode}
              balance={cryptoBalance}
              inputAmount={sellSourceAmount}
              setInputAmount={handleSellSourceAmount}
              setPrimaryButtonEnabled={setPrimaryButtonEnabled}
              isTransactionSource
            />
            <RealTimeCryptoPriceWidget
              currencyCode={currencyCode}
              cryptoPrice={
                cryptoPrice.sell === undefined ? 0.0 : cryptoPrice.sell
              }
              isLoading={isLoading}
            />
            <FeesSummary
              feeAmount={totalFee}
              feeSubTotalAmount={cryptoAmount * cryptoPrice.sell}
              isSellPage
            />
            <CurrencyWidget
              currencyName="Euro"
              currencyCode="EUR"
              balance={fiatBalance}
              inputAmount={cryptoAmount * cryptoPrice.sell - totalFee}
              isTransactionDestination
            />
          </View>
          <View>
            <LoadingSpinner area="buy-button">
              <Button
                block
                primary-light
                style={{
                  marginBottom: 16
                }}
                disabled={!primaryButtonEnabled}
                onPress={async () => {
                  const sellTransaction = await createSellTransaction(user, {
                    customerCode: customerCode,
                    fiatValue: parseFloat(sellSourceAmount),
                    cryptoCode: currencyCode,
                    currencyCode: "EUR",
                    paymentMethodCode: pmCode,
                    accountCode: accountCode,
                    requestedPrice: parseFloat(cryptoPrice.sell)
                  });
                  if (sellTransaction === null) {
                    props.navigation.navigate("ConfirmationScreen", {
                      message: "Sell transaction failed",
                      buttonText: "Try again",
                      destinationScreen: "SellScreen",
                      destinationProps: { user },
                      operationFailed: true
                    });
                  } else {
                    props.navigation.navigate("ConfirmationScreen", {
                      message: "Sell transaction succeeded",
                      destinationScreen: "GenericOverviewScreen", // todo should be currency overview
                      destinationProps: { user }
                    });
                  }
                }}
              >
                <Text>Sell {props.navigation.getParam("currencyName")}</Text>
              </Button>
            </LoadingSpinner>
          </View>
        </Content>
      </Container>
    </SafeAreaView>
  );
};

SellScreen.navigationOptions = ({ navigation }) => {
  return {
    header: (
      <Header darkTheme>
        <Left>
          <Button
            transparent
            onPress={() => {
              navigation.navigate("CurrencyOverviewScreen", {
                currencyCode: navigation.getParam("currencyCode"),
                currencyType: "crypto",
                currencyName: navigation.getParam("currencyName"),
                balance: navigation.getParam("balance"),
                cryptoValue: navigation.getParam("cryptoValue"),
                user: navigation.getParam("user"),
                accountCode: navigation.getParam("accountCode"),
                customerCode: navigation.getParam("customerCode"),
                fiatBalance: navigation.getParam("fiatBalance"),
                cryptoBalance: navigation.getParam("balance")
              });
            }}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Sell {navigation.getParam("currencyName")}</Title>
        </Body>
        <Right />
      </Header>
    )
  };
};

SellScreen.propTypes = {
  currencyName: PropTypes.string,
  currencyCode: PropTypes.string,
  balance: PropTypes.number,
  inputAmount: PropTypes.number,
  setInputAmount: PropTypes.func
};

export default SellScreen;
