import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, Linking } from "react-native";
import {
  Container,
  Header,
  Body,
  Title,
  Right,
  Content,
  Text,
  Button,
  Icon,
  Left,
  ActionSheet,
  Root
} from "native-base";
import TransactionsList from "_components/TransactionsList";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import { signOut } from "_services/Authorization";
import { NavigationActions } from "react-navigation";
import Balance from "_components/Balance";
import { ErrorModal } from "_components/Alert";
import { setRecoveryProps } from "expo/build/ErrorRecovery/ErrorRecovery";
import { getTransactions } from "../services/Server";
import { isLoaded } from "expo-font";

const CurrencyOverviewScreen = props => {
  const [transactions, setTransactions] = useState([]);
  const [user, setUser] = useState(props.navigation.getParam("user", {}));
  const [totalBalance, setTotalBalance] = useState(
    props.navigation.getParam("balance", 0)
  );
  const [code, setCode] = useState(
    props.navigation.getParam("currencyCode", "")
  );
  const [accountCode, setAccountCode] = useState(
    props.navigation.getParam("accountCode", "")
  );
  const [customerCode, setCustomerCode] = useState(
    props.navigation.getParam("customerCode", "")
  );
  const [name, setName] = useState(
    props.navigation.getParam("currencyName", "")
  );
  const [counter, setCounter] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  _isCryptoCurrency = props.navigation.getParam("currencyType") === "crypto";

  useEffect(() => {
    let reload = true;

    if (counter <= 1) {
      setIsLoading(true);
    }

    const fetchData = async () => {
      if (reload) {
        if (!_isCryptoCurrency) {
          setTransactions(
            await getTransactions(user, "", "", code, customerCode)
          );
        } else {
          setTransactions(
            await getTransactions(user, code, accountCode, "EUR", customerCode)
          );
        }
        if (isLoading) {
          setIsLoading(false);
        }
      }
    };

    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };

    var timer = setInterval(tick, 5000);

    fetchData();

    return () => {
      reload = false;
      clearInterval(timer);
    };
  }, [counter]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Balance
          balance={totalBalance}
          user={user}
          navigation={props.navigation}
          type={_isCryptoCurrency ? "crypto-overview" : "fiat-overview"}
          currencyCode={code}
          currencyName={name}
          cryptoValue={props.navigation.getParam("cryptoValue")}
          accountCode={props.navigation.getParam("accountCode")}
          customerCode={customerCode}
          fiatBalance={props.navigation.getParam("fiatBalance")}
          cryptoBalance={props.navigation.getParam("cryptoBalance")}
        />
        <Content padder>
          <Text label-bold-light>Last transactions</Text>
          <TransactionsList
            transactions={transactions}
            currencyType={props.navigation.getParam("currencyType")}
            isLoading={isLoading}
          />
        </Content>
      </Container>
    </SafeAreaView>
  );
};

const _signOutAsync = async navigation => {
  try {
    await trackPromise(signOut(), "sign-out-button");
    navigation.navigate("Auth");
  } catch (error) {
    ErrorModal(error);
  }
};

CurrencyOverviewScreen.navigationOptions = props => {
  return {
    header: (
      <Header darkTheme noBorderRadiusBottom>
        <Left>
          <Button
            transparent
            onPress={() => {
              props.navigation.navigate("GenericOverviewScreen");
            }}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>
            {props.navigation.getParam("currencyName")} (
            {props.navigation.getParam("currencyCode")})
          </Title>
        </Body>
        <Right />
      </Header>
    )
  };
};

export default CurrencyOverviewScreen;
