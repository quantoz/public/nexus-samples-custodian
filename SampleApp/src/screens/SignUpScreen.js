import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, ImageBackground, Image } from "react-native";
import { NavigationActions } from "react-navigation";
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Right,
  Button,
  Label,
  Text,
  Input,
  Form,
  Item,
  Icon
} from "native-base";
import { Row, Grid } from "react-native-easy-grid";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { createAccountAsync } from "_services/Authorization";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import {
  isUserLoggedInAsync,
  getLoggedInUserEmailAsync,
  getUserAsync
} from "_services/Storage";
import { ErrorModal } from "_components/Alert";

const styles = StyleSheet.create({
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  noLeftMargin: {
    marginLeft: 0
  }
});

export default function SignUpScreen({ navigation }) {
  const [email, setEmail] = useState(navigation.getParam("email", ""));
  const [password, setPassword] = useState(navigation.getParam("password", ""));
  const [rePassword, setRePassword] = useState("");
  const [pwEqual, setPwEqual] = useState(false);

  useEffect(() => {
    let reload = true;

    if (reload) {
      setPwEqual(password === rePassword);
    }

    // clean-up the promise
    return () => {
      reload = false;
    };
  }, [password, rePassword]);

  _createAccountAsync = async () => {
    var createAccount = async () => {
      try {
        await createAccountAsync(email.toLowerCase(), password);

        if (await isUserLoggedInAsync()) {
          var userEmail = await getLoggedInUserEmailAsync();
          var user = await getUserAsync(userEmail);
          navigation.navigate("SignUpConfirmed", {
            message: "Account Created",
            buttonText: "Continue",
            destinationScreen: "GenericOverviewScreen",
            destinationProps: { user, userEmail, _isFirstTimeUser: true }
          });
        } else {
          navigation.navigate("SignUpConfirmed", {
            message: "Account not created",
            buttonText: "Try again",
            destinationScreen: "SignUpScreen",
            destinationProps: { user, userEmail, _isFirstTimeUser: true },
            operationFailed: true
          });
        }
      } catch (error) {
        ErrorModal(error);
      }
    };

    await trackPromise(createAccount(), "create-account-button");
  };

  _ifPwEqualCreateAccount = async () => {
    if (pwEqual) {
      await _createAccountAsync();
    } else {
      ErrorModal("Passwords do not match");
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <ImageBackground
          source={require("../assets/images/background_image_onboarding.png")}
          style={styles.backgroundImage}
        >
          <KeyboardAwareScrollView>
            <Grid>
              <Row
                style={{
                  flexDirection: "column",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  paddingVertical: 40
                }}
              >
                <Image
                  source={require("../assets/images/quantoz_logo_white.png")}
                />
                <Text notification-dark style={{ marginTop: 24 }}>
                  Custodian sample app
                </Text>
              </Row>
              <Row style={{ flexDirection: "column", padding: 16 }}>
                <Form>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>Email Address</Label>
                    <Input
                      value={email}
                      onChangeText={setEmail}
                      returnKeyType={"next"}
                      getRef={c => (this.email = c)}
                      onSubmitEditing={() => this.password._root.focus()}
                      blurOnSubmit={false}
                      keyboardType="email-address"
                    />
                  </Item>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>Password</Label>
                    <Input
                      secureTextEntry={true}
                      value={password}
                      onChangeText={setPassword}
                      returnKeyType={"next"}
                      getRef={c => (this.password = c)}
                      onSubmitEditing={() => this.rePassword._root.focus()}
                      blurOnSubmit={false}
                    />
                  </Item>
                  {pwEqual && password !== "" ? (
                    <Item
                      success
                      floatingLabel
                      style={[styles.noLeftMargin, { marginBottom: 24 }]}
                    >
                      <Label>Re-Type Password</Label>
                      <Input
                        secureTextEntry={true}
                        value={rePassword}
                        onChangeText={setRePassword}
                        returnKeyType={"next"}
                        getRef={c => (this.rePassword = c)}
                        onSubmitEditing={() => {
                          _ifPwEqualCreateAccount();
                        }}
                        blurOnSubmit={true}
                      />
                      <Icon name="checkmark-circle" />
                    </Item>
                  ) : (
                    <Item
                      error
                      floatingLabel
                      style={[styles.noLeftMargin, { marginBottom: 24 }]}
                    >
                      <Label>Re-Type Password</Label>
                      <Input
                        secureTextEntry={true}
                        value={rePassword}
                        onChangeText={setRePassword}
                        returnKeyType={"next"}
                        getRef={c => (this.rePassword = c)}
                        onSubmitEditing={() => {
                          _ifPwEqualCreateAccount();
                        }}
                        blurOnSubmit={true}
                      />
                      <Icon name="close-circle" />
                    </Item>
                  )}
                  <LoadingSpinner area="create-account-button">
                    <Button
                      block
                      primary-dark
                      onPress={() => {
                        _ifPwEqualCreateAccount();
                      }}
                    >
                      <Text>Create Account</Text>
                    </Button>
                  </LoadingSpinner>
                </Form>
              </Row>
            </Grid>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </Container>
    </SafeAreaView>
  );
}

SignUpScreen.navigationOptions = ({ navigation }) => {
  return {
    header: (
      <Header>
        <Left>
          <Button
            transparent
            onPress={() => navigation.dispatch(NavigationActions.back())}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Sign up</Title>
        </Body>
        <Right />
      </Header>
    )
  };
};
