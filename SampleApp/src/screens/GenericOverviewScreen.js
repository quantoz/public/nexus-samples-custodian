import React, { useState, useEffect, useRef } from "react";
import {
  Root,
  Container,
  Content,
  Header,
  Left,
  Body,
  Title,
  Right,
  Icon,
  Button
} from "native-base";
import { SafeAreaView, Alert } from "react-native";
import Balance from "_components/Balance";
import BalanceList from "_components/BalanceList";
import { signOut } from "_services/Authorization";
import { ErrorModal } from "_components/Alert";
import { trackPromise } from "_components/LoadingSpinner";
import { getBalance, getPrice, getCustomer } from "../services/Server";

const showLegalAlert = () =>
  Alert.alert(
    "Disclaimer & Privacy Policy",
    `You acknowledge and agree in relation to any downloads which are accessed on or via the Quantoz website (“Download”) that \n
    (i) you have a right of license to such Download, you do not own any intellectual property rights in such Download and your use of such Download may be subject to an applicable end user licence agreement; \n
    (ii) you are not permitted to modify, copy, commercially exploit, resell or otherwise use a Download in any way which is contrary to applicable laws or regulations. Without limiting the foregoing, you expressly agree that Quantoz, its officers, subsidiaries, employees and/or affiliates shall have no liability whatsoever for the performance of any software product you purchase and/or download through, or via a link from, the Quantoz website (“Download”). You expressly acknowledge and agree that use of any Download is at your sole risk and that the entire risk as to satisfactory quality, performance, accuracy and effort is with you. To the maximum extent permitted by applicable law, Downloads are provided “As Is” and without warranty of any kind, and Quantoz and its group companies hereby disclaim all warranties and conditions with respect to the Download, either express, implied or statutory, including (without limitation) the implied warranties and/or conditions of merchantability, satisfactory quality, fitness for a particular purpose, accuracy, quiet enjoyment and noninfringement of third party rights. Quantoz does not warrant against interference with your enjoyment of the Download, that the functions contained in the Download will be continuous, uninterrupted, secure, virus-free, or error-free, or that defects in the Download will be corrected. Should the Download prove defective, you assume the entire cost of all necessary servicing, repair or correction.\n\n
    Privacy Policy\n
    1. Information collection\n
    When using the sample app, we ask certain information from you:\n
    “Personal Information” We don't collect any personal data, except the email addresses and information you submitted voluntarily.\n
    2. Information usage\n
    Your information will not be shared with others and is only used internally for the purposes described below: to provide our services or information you request, and to process and complete any transactions; to send you confirmations, updates, security alerts, and support and administrative messages and otherwise facilitate your use of, and our administration and operation of, our services.`,
    [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );

const showWelcomeAlert = () =>
  Alert.alert(
    "Welcome to the Custodian app",
    `For demo purposes, an amount of 100 virtual Euro has been transferred to the EUR account, enabling you to buy, sell or swap  crypto in this sample app.`,
    [{ text: "OK" }],
    { cancelable: false }
  );

const calculateTotalBalance = (prices, balances, euroBalance) => {
  let total = 0;
  balances.forEach(b => {
    total = total + prices.find(c => c.code === b.code).buy * b.total;
  });
  return total + euroBalance;
};

const GenericOverviewScreen = ({ navigation }) => {
  const _isFirstTimeUser = navigation.getParam("_isFirstTimeUser", false);
  const [userEmail, setUserEmail] = useState(
    navigation.getParam("userEmail", "")
  );
  const [user, setUser] = useState(navigation.getParam("user", {}));
  const [customer, setCustomer] = useState([]);
  const [cryptoBalance, setCryptoBalance] = useState([]);
  const [currencyBalance, setCurrencyBalance] = useState([]);
  const [totalBalance, setTotalBalance] = useState(0);
  const [cryptoPrices, setPrices] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [fiatBalance, setFiatBalance] = useState(0.0);
  const customerCode = userEmail
    .replace(/@/g, "_at_")
    .replace(/\./g, "_dot_")
    .toLocaleUpperCase();

  const [counter, setCounter] = useState(1);

  useEffect(() => {
    if (_isFirstTimeUser) showWelcomeAlert();
  }, []);

  useEffect(() => {
    let mounted = true;
    const fetchData = async () => {
      if (mounted) {
        setCustomer(await getCustomer(user, customerCode));
      }
    };

    fetchData();

    return () => {
      mounted = false;
    };
  }, []);

  useEffect(() => {
    let reload = true;

    console.log("[GenericOverviewScreen.js] - fetchBalances useEffect");

    const fetchData = async () => {
      if (counter <= 1) {
        setIsLoading(true);
      }

      if (reload) {
        const customerBalance = await getBalance(user, customerCode);
        setCryptoBalance(customerBalance.cryptoBalances);
        setCurrencyBalance(customerBalance.currencyBalances);
        setFiatBalance(customerBalance.currencyBalances[0].total + 100);
        setPrices(await getPrice(user));

        if (cryptoPrices !== undefined && cryptoBalance !== undefined) {
          setTotalBalance(
            calculateTotalBalance(cryptoPrices, cryptoBalance, fiatBalance)
          );
        }
        if (isLoading) {
          setIsLoading(false);
        }
      }
    };
    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };
    var timer = setInterval(tick, 5000);

    fetchData();

    return () => {
      clearInterval(timer);
      reload = false;
    };
  }, [counter]);

  console.log("[GenericOverviewScreen.js] - rendering");

  return (
    <Root>
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <Balance
            type="generic_overview"
            isLoading={isLoading}
            fiatBalance={totalBalance}
          ></Balance>
          <Content padder>
            <BalanceList
              list={currencyBalance}
              type="fiat"
              customer={customer}
              user={user}
              navigation={navigation}
              isLoading={isLoading}
              customerCode={customerCode}
              fiatBalance={fiatBalance}
            />
            <BalanceList
              list={cryptoBalance}
              customer={customer}
              user={user}
              type="crypto"
              navigation={navigation}
              isLoading={isLoading}
              prices={cryptoPrices}
              customerCode={customerCode}
              fiatBalance={fiatBalance}
            />
          </Content>
        </Container>
      </SafeAreaView>
    </Root>
  );
};

const _signOutAsync = async navigation => {
  try {
    await trackPromise(signOut(), "sign-out-button");
    navigation.navigate("Auth");
  } catch (error) {
    ErrorModal(error);
  }
};

GenericOverviewScreen.navigationOptions = ({ navigation }) => {
  return {
    header: (
      <Header noLeft darkTheme noBorderRadiusBottom>
        <Left />
        <Body>
          <Title>Nexus Custodian</Title>
        </Body>
        <Right>
          <Button transparent onPress={showLegalAlert}>
            <Icon name="information-circle-outline" />
          </Button>
          <Button transparent onPress={async () => _signOutAsync(navigation)}>
            <Icon name="log-out" />
          </Button>
        </Right>
      </Header>
    )
  };
};

export default GenericOverviewScreen;
