import React, { useState, useEffect } from "react";
import { SafeAreaView } from "react-native";
import {
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Title,
  Content,
  Container,
  View,
  Text
} from "native-base";
import { TransitionPresets } from "react-navigation-stack";
import CurrencyWidget from "_components/CurrencyWidget";
import RealTimeCryptoPriceWidget from "_components/RealTimeCryptoPriceWidget";
import InfoBox from "_components/InfoBox";
import {
  createSwapTransaction,
  getPaymentMethodCode,
  getCryptoPrice,
  getBalance
} from "../services/Server";

const calculateTotalSwapTransactionAmount = (
  amount,
  sourceCryptoSellPrice,
  destinationCryptoBuyPrice
) => {
  const sourceTotal = amount * sourceCryptoSellPrice;
  return destinationCryptoBuyPrice > 0
    ? sourceTotal / destinationCryptoBuyPrice
    : 0.0;
};

const SwapScreen = props => {
  const [swapSourceAmount, setSwapSourceAmount] = useState(0.0);
  const [swapDestinationAmount, setSwapDestinationAmount] = useState(0.0);
  const [sourceCryptoSellPrice, setSourceCryptoSellPrice] = useState(0.0);
  const [destinationCryptoBuyPrice, setDestinationCryptoBuyPrice] = useState(
    0.0
  );
  const [chosenSwapCrypto, setChosenSwapCrypto] = useState({
    name: "Select crypto",
    code: "",
    balance: "-",
    accountCode: ""
  });
  const [primaryButtonEnabled, setPrimaryButtonEnabled] = useState(false);

  const [user, setUser] = useState(props.navigation.getParam("user", {}));
  const [balance, setBalance] = useState(
    props.navigation.getParam("balance", 0.0)
  );
  const [currencyCode, setCurrencyCode] = useState(
    props.navigation.getParam("currencyCode"),
    ""
  );
  const [pmCode, setPmCode] = useState(
    getPaymentMethodCode(currencyCode, "SWAP")
  );
  const [customerCode, setCustomerCode] = useState(
    props.navigation.getParam("customerCode", "")
  );
  const [accountCode, setAccountCode] = useState(
    props.navigation.getParam("accountCode", "")
  );

  const [counter, setCounter] = useState(1);

  const handleSwapSourceAmount = amount => {
    // NEEDED: local calculations to avoid async state update issues
    const _totalSwapAmount = calculateTotalSwapTransactionAmount(
      amount,
      sourceCryptoSellPrice,
      destinationCryptoBuyPrice
    );

    setSwapSourceAmount(amount);
    if (chosenSwapCrypto.code !== "") {
      setSwapDestinationAmount(_totalSwapAmount);
    }

    if (amount > balance || amount == 0 || chosenSwapCrypto.code === "") {
      setPrimaryButtonEnabled(false);
    } else {
      setPrimaryButtonEnabled(true);
    }
  };

  useEffect(() => {
    console.log("Swap screen ~ render ~ hook 1");

    let reload = true;

    const fetchData = async () => {
      if (reload) {
        console.log("Swap screen ~ render ~ hook 1 ~ fetchData");
        const _sourceCryptoPrice = await getCryptoPrice(user, currencyCode);
        setSourceCryptoSellPrice(_sourceCryptoPrice.sell);

        if (chosenSwapCrypto.code !== "") {
          const _destCryptoPrice = await getCryptoPrice(
            user,
            chosenSwapCrypto.code
          );
          setDestinationCryptoBuyPrice(_destCryptoPrice.buy);
          console.log("destination crypto price", _destCryptoPrice);

          const _totalSwapAmount = calculateTotalSwapTransactionAmount(
            swapSourceAmount,
            sourceCryptoSellPrice,
            destinationCryptoBuyPrice
          );

          if (chosenSwapCrypto.code !== "") {
            setSwapDestinationAmount(_totalSwapAmount);
          }

          if (swapSourceAmount > balance || swapSourceAmount == 0) {
            setPrimaryButtonEnabled(false);
          } else {
            setPrimaryButtonEnabled(true);
          }
        }
      }
    };

    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };

    var timer = setInterval(tick, 1000);

    fetchData();

    return () => {
      reload = false;
      clearInterval(timer);
    };
  }, [counter, chosenSwapCrypto.code]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content padder>
          <InfoBox
            title="What does swapping means?"
            message="When swapping two cryptocurrencies, you are actually selling the source crypto and buying the second one. Hence the visualization of the two separate prices."
          />
          <View>
            <CurrencyWidget
              currencyName={props.navigation.getParam("currencyName")}
              currencyCode={props.navigation.getParam("currencyCode")}
              balance={balance}
              inputAmount={swapSourceAmount}
              setInputAmount={handleSwapSourceAmount}
              setPrimaryButtonEnabled={setPrimaryButtonEnabled}
              isTransactionSource
            />
            <RealTimeCryptoPriceWidget
              currencyCode={props.navigation.getParam("currencyCode")}
              secondCurrencyCode={chosenSwapCrypto.code}
              cryptoPrice={sourceCryptoSellPrice}
              secondCryptoPrice={destinationCryptoBuyPrice}
            />

            <CurrencyWidget
              currencyName={chosenSwapCrypto.name}
              currencyCode={chosenSwapCrypto.code}
              balance={chosenSwapCrypto.balance}
              inputAmount={swapDestinationAmount}
              isTransactionDestination
              isSwapOperation
              customerCode={customerCode}
              user={props.navigation.getParam("user")}
              sourceCurrencyCode={currencyCode}
              setChosenSwapCrypto={setChosenSwapCrypto}
            />
          </View>
          <View>
            <Button
              block
              primary-light
              style={{
                marginBottom: 16
              }}
              disabled={!primaryButtonEnabled}
              onPress={async () => {
                const swapTransaction = await createSwapTransaction(user, {
                  customerCode: customerCode,
                  paymentMethodCode: pmCode,
                  source: {
                    accountCode: accountCode,
                    cryptoCode: currencyCode,
                    cryptoAmount: parseFloat(swapSourceAmount),
                    requestedPrice: parseFloat(sourceCryptoSellPrice)
                  },
                  destination: {
                    accountCode: chosenSwapCrypto.accountCode,
                    cryptoCode: chosenSwapCrypto.code,
                    requestedPrice: parseFloat(destinationCryptoBuyPrice)
                  }
                });
                if (swapTransaction === null) {
                  props.navigation.navigate("ConfirmationScreen", {
                    message: "Swap transaction failed",
                    buttonText: "Try again",
                    destinationScreen: "SwapScreen",
                    destinationProps: { user },
                    operationFailed: true
                  });
                } else {
                  props.navigation.navigate("ConfirmationScreen", {
                    message: "Swap transaction succeeded",
                    destinationScreen: "GenericOverviewScreen", // todo should be currency overview
                    destinationProps: { user }
                  });
                }
              }}
            >
              <Text>
                Swap {currencyCode}{" "}
                {chosenSwapCrypto.code ? "for " + chosenSwapCrypto.code : ""}
              </Text>
            </Button>
          </View>
        </Content>
      </Container>
    </SafeAreaView>
  );
};

SwapScreen.navigationOptions = ({ navigation }) => {
  const { goBack } = navigation;
  return {
    // ...TransitionPresets.SlideFromRightIOS,
    // gestureDirection: 'horizontal-inverted',
    header: (
      <Header darkTheme>
        <Left>
          <Button transparent onPress={() => goBack()}>
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Swap {navigation.getParam("currencyName")}</Title>
        </Body>
        <Right />
      </Header>
    )
  };
};

export default SwapScreen;
