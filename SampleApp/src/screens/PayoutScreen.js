import React, { useState } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import {
  Header,
  Left,
  Body,
  Right,
  Title,
  Button,
  Label,
  Text,
  Input,
  Item,
  Icon,
  Container,
  Content,
  Form
} from "native-base";
import { NavigationActions } from "react-navigation";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import Config from "Config";
import { createPayout, submitTransactionAsync } from "_services/Server";
import { signTransaction } from "_services/Stellar";
import { ErrorModal } from "_components/Alert";

const styles = StyleSheet.create({
  noLeftMargin: {
    marginLeft: 0
  },
  lastInput: {
    marginBottom: 24
  }
});

export default function PayoutScreen({ navigation }) {
  const [user, setUser] = useState(navigation.getParam("user", {}));
  const [amount, setAmount] = useState("");
  const [token, setToken] = useState(Config.DEFAULT_ASSET);

  const _createPayoutAsync = async () => {
    const doCreatePayout = async () => {
      try {
        var response = await createPayout(user, {
          fromAddress: user.keypair.publicKey,
          tokenCode: token,
          amount
        });

        // response should be:
        // "signingNeeded": true,
        // "transactionEnvelope": ""
        const signedTx = signTransaction(
          response.transactionEnvelope,
          user.keypair.secret
        );
        await submitTransactionAsync(user, signedTx);

        navigation.navigate("PayoutConfirmed", {
          message: "Cashout on its way!",
          destinationScreen: "CurrencyOverviewScreen",
          destinationProps: { user }
        });
      } catch (error) {
        ErrorModal(error.message);
      }
    };

    await trackPromise(doCreatePayout(), "create-payout-button");
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content padder>
          <Form>
            <Item floatingLabel floatingLabel-light style={styles.noLeftMargin}>
              <Label>Amount</Label>
              <Input
                value={amount}
                onChangeText={setAmount}
                returnKeyType={"next"}
                getRef={c => (this.amount = c)}
                onSubmitEditing={() => _createPayoutAsync()}
                blurOnSubmit={false}
                keyboardType="decimal-pad"
              />
            </Item>
            <Item
              floatingLabel
              floatingLabel-light
              style={[styles.noLeftMargin, styles.lastInput]}
            >
              <Label>Token</Label>
              <Input disabled value={token} />
            </Item>
          </Form>
          <LoadingSpinner area="create-payout-button">
            <Button
              block
              primary-light
              onPress={() => {
                _createPayoutAsync();
              }}
            >
              <Text button>Cash out</Text>
            </Button>
          </LoadingSpinner>
        </Content>
      </Container>
    </SafeAreaView>
  );
}

PayoutScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header darkTheme>
      <Left>
        <Button
          transparent
          onPress={() => navigation.dispatch(NavigationActions.back())}
        >
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Title>Cash out</Title>
      </Body>
      <Right />
    </Header>
  )
});
