import React, { useState, useEffect } from "react";
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  Content,
  Text,
  View
} from "native-base";
import { CancelToken } from "axios";
import CurrencyWidget from "_components/CurrencyWidget";
import { SafeAreaView } from "react-native";
import PropTypes from "prop-types";
import RealTimeCryptoPriceWidget from "_components/RealTimeCryptoPriceWidget";
import FeesSummary from "_components/FeesSummary";
import { LoadingSpinner } from "_components/LoadingSpinner";
import {
  createBuyTransaction,
  getPaymentMethodCode,
  getCryptoPrice,
  getBuyTransactionFee
} from "../services/Server";

const BuyScreen = props => {
  const [buySourceAmount, setBuySourceAmount] = useState(0.0);
  const [cryptoPrice, setPrice] = useState({});
  const [totalFee, setTotalFee] = useState(0.0);
  const [feeSubTotalAmount, setFeeSubTotalAmount] = useState(0.0);
  const [cryptoAmount, setCryptoAmount] = useState(0.0);
  const [primaryButtonEnabled, setPrimaryButtonEnabled] = useState(false);
  const [user, setUser] = useState(props.navigation.getParam("user", {}));
  const [fiatBalance, setFiatBalance] = useState(
    props.navigation.getParam("fiatBalance", 0.0)
  );
  const [cryptoBalance, setCryptoBalance] = useState(
    props.navigation.getParam("balance", 0.0)
  );
  const [currencyCode, setCurrencyCode] = useState(
    props.navigation.getParam("currencyCode"),
    ""
  );
  const [pmCode, setPmCode] = useState(
    getPaymentMethodCode(currencyCode, "BUY")
  );
  const [customerCode, setCustomerCode] = useState(
    props.navigation.getParam("customerCode", "")
  );
  const [accountCode, setAccountCode] = useState(
    props.navigation.getParam("accountCode", "")
  );
  const [isLoading, setIsLoading] = useState(false);
  const [counter, setCounter] = useState(1);

  useEffect(() => {
    let reload = true;

    const fetchData = async () => {
      if (counter <= 1) {
        setIsLoading(true);
      }

      if (reload) {
        setPrice(await getCryptoPrice(user, currencyCode));
        if (isLoading) {
          setIsLoading(false);
        }
      }
    };

    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };

    var timer = setInterval(tick, 1000);

    fetchData();

    return () => {
      reload = false;
      clearInterval(timer);
    };
  }, [counter]);

  useEffect(() => {
    console.log("Buy screen ~ render ~ hook 2");
    let reload = true;

    const fetchData = async () => {
      if (reload) {
        if (buySourceAmount > 0) {
          const finalFeeAndCryptoAmount = await getBuyTransactionFee(user, {
            customerCode: customerCode,
            fiatValue: parseFloat(buySourceAmount),
            cryptoCode: currencyCode,
            currencyCode: "EUR",
            paymentMethodCode: pmCode,
            accountCode: accountCode,
            requestedPrice: parseFloat(cryptoPrice.buy)
          });
          if (finalFeeAndCryptoAmount !== null) {
            setTotalFee(finalFeeAndCryptoAmount.fee);
            setCryptoAmount(finalFeeAndCryptoAmount.cryptoAmount);
          }
        } else {
          setTotalFee(0.0);
          setCryptoAmount(0.0);
        }
      }
    };
    fetchData();

    return () => {
      reload = false;
    };
  }, [buySourceAmount, counter]);

  const handleBuySourceAmount = amount => {
    if (amount === "") {
      setBuySourceAmount(0.0);
    } else {
      setBuySourceAmount(amount);
    }

    const _totalBuyAmount = buySourceAmount - totalFee;
    setFeeSubTotalAmount(_totalBuyAmount);

    if (
      amount === 0 ||
      amount === "" ||
      fiatBalance === 0 ||
      amount > fiatBalance
    ) {
      setPrimaryButtonEnabled(false);
    } else {
      setPrimaryButtonEnabled(true);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content padder>
          <View>
            <CurrencyWidget
              currencyName="Euro"
              currencyCode="EUR"
              balance={fiatBalance}
              inputAmount={buySourceAmount}
              setInputAmount={handleBuySourceAmount}
              setPrimaryButtonEnabled={setPrimaryButtonEnabled}
              isTransactionSource
            />
            <FeesSummary
              feeAmount={totalFee}
              feeSubTotalAmount={buySourceAmount - totalFee}
            />
            <RealTimeCryptoPriceWidget
              currencyCode={currencyCode}
              cryptoPrice={
                cryptoPrice.buy === undefined ? 0.0 : cryptoPrice.buy
              }
              isLoading={isLoading}
            />
            <CurrencyWidget
              currencyName={props.navigation.getParam("currencyName")}
              currencyCode={currencyCode}
              balance={cryptoBalance}
              inputAmount={cryptoAmount}
              isTransactionDestination
            />
          </View>
          <View>
            <LoadingSpinner area="buy-button">
              <Button
                block
                primary-light
                style={{
                  marginBottom: 16
                }}
                disabled={!primaryButtonEnabled}
                onPress={async () => {
                  const buyTransaction = await createBuyTransaction(user, {
                    customerCode: customerCode,
                    fiatValue: parseFloat(buySourceAmount),
                    cryptoCode: currencyCode,
                    currencyCode: "EUR",
                    paymentMethodCode: pmCode,
                    accountCode: accountCode,
                    requestedPrice: parseFloat(cryptoPrice.buy)
                  });
                  if (buyTransaction === null) {
                    props.navigation.navigate("ConfirmationScreen", {
                      message: "Buy transaction failed",
                      buttonText: "Try again",
                      destinationScreen: "BuyScreen",
                      destinationProps: { user },
                      operationFailed: true
                    });
                  } else {
                    props.navigation.navigate("ConfirmationScreen", {
                      message: "Buy transaction succeeded",
                      destinationScreen: "GenericOverviewScreen", // todo should be currency overview
                      destinationProps: { user, cryptoBalance }
                    });
                  }
                }}
              >
                <Text>Buy {props.navigation.getParam("currencyName")}</Text>
              </Button>
            </LoadingSpinner>
          </View>
        </Content>
      </Container>
    </SafeAreaView>
  );
};

BuyScreen.navigationOptions = ({ navigation }) => {
  return {
    header: (
      <Header darkTheme>
        <Left>
          <Button
            transparent
            onPress={() => {
              navigation.navigate("CurrencyOverviewScreen", {
                currencyCode: navigation.getParam("currencyCode"),
                currencyType: "crypto",
                currencyName: navigation.getParam("currencyName"),
                balance: navigation.getParam("balance"),
                cryptoValue: navigation.getParam("cryptoValue"),
                user: navigation.getParam("user"),
                accountCode: navigation.getParam("accountCode"),
                customerCode: navigation.getParam("customerCode"),
                fiatBalance: navigation.getParam("fiatBalance"),
                cryptoBalance: navigation.getParam("balance")
              });
            }}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Buy {navigation.getParam("currencyName")}</Title>
        </Body>
        <Right />
      </Header>
    )
  };
};

BuyScreen.propTypes = {
  currencyName: PropTypes.string,
  currencyCode: PropTypes.string,
  cryptoBalance: PropTypes.number,
  inputAmount: PropTypes.number,
  setInputAmount: PropTypes.func
};

export default BuyScreen;
