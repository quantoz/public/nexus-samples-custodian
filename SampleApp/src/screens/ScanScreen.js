"use strict";

import React, { Component } from "react";

import { StyleSheet, TouchableOpacity } from "react-native";
import { Text } from "native-base";

import QRCodeScanner from "react-native-qrcode-scanner";

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: "#777"
  },
  textBold: {
    fontWeight: "500",
    color: "#000"
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  }
});

export default function ScanScreen({ navigation }) {
  const onScan = navigation.getParam("onScan", () => {});

  return (
    <QRCodeScanner
      onRead={qr => {
        onScan(qr.data);
        navigation.goBack();
      }}
      topContent={
        <Text style={styles.centerText}>Scan the account address QR code.</Text>
      }
      bottomContent={
        <TouchableOpacity
          style={styles.buttonTouchable}
          onPress={() => navigation.goBack()}
        >
          <Text style={styles.buttonText}>Back</Text>
        </TouchableOpacity>
      }
    />
  );
}
