// Copied from Transaction.js, check that file for the logic

import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { ListItem, Left, Body, Text, Right, Thumbnail } from "native-base";
import {
  btc,
  bch,
  eth,
  ltc,
  xlm,
  xrp,
  eur,
  unk
} from "_services/CurrencyLogos";

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "#fff",
    paddingRight: 0,
    height: 72
  },
  noLeftMargin: {
    marginLeft: 0
  },
  icon: {
    color: "#02182B"
  },
  currencyIcon: {
    width: 40,
    height: 40
  }
});

renderSwitch = param => {
  switch (param) {
    case "BTC":
      return btc;
    case "BCH":
      return bch;
    case "ETH":
      return eth;
    case "LTC":
      return ltc;
    case "XLM":
      return xlm;
    case "XRP":
      return xrp;
    case "EUR":
      return eur;
    default:
      return unk;
  }
};

const handleListItemClick = (props, user) => {
  if (props.fromCryptoPickerScreen) {
    props.setChosenSwapCrypto({
      code: props.code,
      name: props.name,
      balance: props.balance,
      accountCode: props.navigation.getParam("accountCode")
    });
    props.navigation.navigate("SwapScreen", {
      user: user
    });
  } else {
    props.navigation.navigate("CurrencyOverviewScreen", {
      currencyCode: props.code,
      currencyType: props.currencyType,
      currencyName: props.name,
      balance: props.balance,
      cryptoValue: props.cryptoValue,
      user: user,
      accountCode: props.accountCode,
      customerCode: props.customerCode,
      fiatBalance: props.fiatBalance,
      cryptoBalance: props.cryptoBalance
    });
  }
};

const BalanceListItem = props => {
  const [user, setUser] = useState(props.navigation.getParam("user", {}));
  return (
    <ListItem
      noIndent
      icon
      style={styles.listItem}
      onPress={() => handleListItemClick(props, user)}
    >
      <Left>
        <Thumbnail
          source={renderSwitch(props.code)}
          style={styles.currencyIcon}
        />
      </Left>
      <Body>
        <Text body-bold-light>{props.name}</Text>
        {props.currencyType === "crypto" ? (
          <Text note>
            1 {props.code} = EUR {props.euroAmount.toFixed(5)}
          </Text>
        ) : null}
      </Body>
      <Right
        multilines={props.currencyType === "crypto"}
        verticalCenter={props.currencyType === "fiat"}
      >
        {props.currencyType === "crypto" ? (
          <Text body-bold-light>
            {props.code + " " + props.balance.toFixed(5)}
          </Text>
        ) : (
          <Text body-bold-light>
            {props.code + " " + props.balance.toFixed(2)}
          </Text>
        )}
        {props.currencyType === "crypto" ? (
          <Text note>EUR {(props.balance * props.euroAmount).toFixed(2)}</Text>
        ) : null}
      </Right>
    </ListItem>
  );
};

export default BalanceListItem;
