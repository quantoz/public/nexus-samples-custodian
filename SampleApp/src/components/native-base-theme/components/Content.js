// @flow

export default () => {
  const contentTheme = {
    flex: 1,
    backgroundColor: "#FFF",
    "NativeBase.Segment": {
      borderWidth: 0,
      backgroundColor: "transparent"
    }
  };

  return contentTheme;
};
