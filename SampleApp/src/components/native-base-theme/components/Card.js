// @flow

import variable from "./../variables/platform";
import { Colors } from "_styles";

export default (variables /* : * */ = variable) => {
  const cardTheme = {
    ".transparent": {
      shadowColor: null,
      shadowOffset: null,
      shadowOpacity: null,
      shadowRadius: null,
      elevation: null,
      backgroundColor: Colors.background,
      borderWidth: 0
    },
    ".noShadow": {
      shadowColor: null,
      shadowOffset: null,
      shadowOpacity: null,
      elevation: null
    },
    marginBottom: 16,
    // marginHorizontal: 16,
    borderWidth: variables.borderWidth,
    borderRadius: variables.cardBorderRadius,
    borderColor: variables.curiousBlueColor,
    // flexWrap: 'nowrap',
    backgroundColor: variables.curiousBlueColor,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.25,
    shadowRadius: 1.5,
    elevation: 5
  };

  return cardTheme;
};
