import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Text, View } from "native-base";
import { Grid, Col, Row } from "react-native-easy-grid";

const styles = StyleSheet.create({
  verticalCenter: {
    justifyContent: "center"
  },
  verticalPadding: {
    paddingVertical: 16
  }
});

const FeesSummary = props => {
  const sellPageFees = () => {
    return (
      <Grid style={styles.verticalPadding}>
        <Row>
          <Col style={styles.verticalCenter}>
            <Text label-light>Subtotal</Text>
          </Col>
          <Col style={[styles.verticalCenter, { alignItems: "flex-end" }]}>
            <Text notification-light-regular>
              EUR {props.feeSubTotalAmount.toFixed(2)}
            </Text>
          </Col>
        </Row>
        <Row>
          <Col style={styles.verticalCenter}>
            <Text label-light>Fees</Text>
          </Col>
          <Col style={[styles.verticalCenter, { alignItems: "flex-end" }]}>
            <Text notification-light-regular>
              EUR {-props.feeAmount.toFixed(2)}
            </Text>
          </Col>
        </Row>
      </Grid>
    );
  };

  const buyPageFees = () => {
    return (
      <Grid style={styles.verticalPadding}>
        <Row>
          <Col style={styles.verticalCenter}>
            <Text label-light>Fees</Text>
          </Col>
          <Col style={[styles.verticalCenter, { alignItems: "flex-end" }]}>
            <Text notification-light-regular>
              EUR {props.feeAmount.toFixed(2)}
            </Text>
          </Col>
        </Row>
        <Row>
          <Col style={styles.verticalCenter}>
            <Text label-light>Subtotal</Text>
          </Col>
          <Col style={[styles.verticalCenter, { alignItems: "flex-end" }]}>
            <Text notification-light-regular>
              EUR {props.feeSubTotalAmount.toFixed(2)}
            </Text>
          </Col>
        </Row>
      </Grid>
    );
  };
  return props.isSellPage ? sellPageFees() : buyPageFees();
};

export default FeesSummary;
