import React from "react";
import { StyleSheet } from "react-native";
import { Text, Button, Icon, View } from "native-base";
import { Col, Grid } from "react-native-easy-grid";
import variables from "./native-base-theme/variables/platform";

const styles = StyleSheet.create({
  buttonContainer: {
    paddingVertical: 16,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  separator: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: variables.submarineColor
  }
});

const RealTimeCryptoPriceWidget = props => {
  let currentPriceFirst = <Text label-light>Updating...</Text>;
  let currentPriceSecond = <Text label-light>Updating...</Text>;

  if (!props.isLoading) {
    currentPriceFirst = (
      <Text label-light>
        1 {props.currencyCode} = EUR {props.cryptoPrice.toFixed(5)} (ask)
      </Text>
    );
    if (props.secondCurrencyCode) {
      currentPriceSecond = (
        <Text label-light>
          1 {props.secondCurrencyCode} = EUR{" "}
          {props.secondCryptoPrice.toFixed(5)} (bid)
        </Text>
      );
    }
  }

  return (
    <Grid style={styles.buttonContainer}>
      <Col size={1} style={styles.separator}>
        <View />
      </Col>
      <Col size={9}>
        <Button iconLeft cryptoPrice>
          <Icon name="trending-up" />
          <Text label-light>{currentPriceFirst}</Text>
        </Button>
        {props.secondCurrencyCode ? (
          <Button iconLeft cryptoPrice>
            <Icon name="trending-up" />
            <Text label-light>{currentPriceSecond}</Text>
          </Button>
        ) : null}
      </Col>
      <Col size={1} style={styles.separator}>
        <View />
      </Col>
    </Grid>
  );
};

export default RealTimeCryptoPriceWidget;
