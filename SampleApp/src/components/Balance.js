import React, { useState } from "react";
import { StyleSheet, View, Alert } from "react-native";
import { Text, Button, Spinner } from "native-base";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";

const localStyles = StyleSheet.create({
  balanceArea: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 32,
    paddingBottom: 32,
    backgroundColor: "#02182B",
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8
  },
  balanceText: {
    paddingTop: 8
  },
  buttonContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingTop: 24
  }
});

const contactDemoAlert = () =>
  Alert.alert(
    "Contact us for a demo",
    `Funding and withdrawing have not been implemented in this sample application.\nContact us for a demo of our platform or refer to the Nexus documentation to see how to implement these features`,
    [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );

const FiatButtons = (
  <View style={localStyles.buttonContainer}>
    <Button primary-light onPress={() => contactDemoAlert()}>
      <Text button>Fund</Text>
    </Button>
    <Button primary-light onPress={() => contactDemoAlert()}>
      <Text button>Withdraw</Text>
    </Button>
  </View>
);

const CryptoButtons = props => {
  return (
    <View style={localStyles.buttonContainer}>
      <Button
        primary-light
        short
        onPress={() => {
          props.navigation.navigate("BuyScreen", {
            currencyCode: props.currencyCode,
            currencyName: props.currencyName,
            user: props.user,
            balance: props.balance,
            accountCode: props.accountCode,
            customerCode: props.customerCode,
            fiatBalance: props.fiatBalance,
            cryptoValue: props.cryptoValue
          });
        }}
      >
        <Text button>Buy</Text>
      </Button>
      <Button
        primary-light
        short
        disabled={props.balance <= 0}
        onPress={() => {
          props.navigation.navigate("SellScreen", {
            currencyCode: props.currencyCode,
            currencyName: props.currencyName,
            user: props.user,
            balance: props.balance,
            accountCode: props.accountCode,
            customerCode: props.customerCode,
            fiatBalance: props.fiatBalance,
            cryptoValue: props.cryptoValue
          });
        }}
      >
        <Text button>Sell</Text>
      </Button>
      <Button
        primary-light
        short
        disabled={props.balance <= 0}
        onPress={() => {
          props.navigation.navigate("SwapScreen", {
            currencyCode: props.currencyCode,
            currencyName: props.currencyName,
            user: props.user,
            balance: props.balance,
            accountCode: props.accountCode,
            customerCode: props.customerCode,
            fiatBalance: props.fiatBalance,
            cryptoValue: props.cryptoValue
          });
        }}
      >
        <Text button>Swap</Text>
      </Button>
    </View>
  );
};

const ButtonsSwitch = props => {
  const currencyType = props.type;
  switch (currencyType) {
    case "fiat-overview":
      return FiatButtons;
    case "crypto-overview":
      return CryptoButtons(props);
    default:
      return null;
  }
};

export default Balance = props => {
  return (
    <View style={localStyles.balanceArea}>
      {props.type === "generic_overview" ? (
        <Text label-bold-dark>Total balance</Text>
      ) : null}
      {props.isLoading ? (
        <Spinner size="large" />
      ) : props.type === "generic_overview" ? (
        <Text balance-dark style={localStyles.balanceText}>
          EUR {props.fiatBalance.toFixed(2)}
        </Text>
      ) : (
        <Text balance-dark style={localStyles.balanceText}>
          {props.currencyCode}{" "}
          {props.currencyCode === "EUR"
            ? props.balance.toFixed(2)
            : props.navigation.getParam("cryptoBalance")
            ? props.navigation.getParam("cryptoBalance").toFixed(5)
            : props.balance.toFixed(5)}
        </Text>
      )}
      {props.type === "crypto-overview" ? (
        <Text label-bold-dark>EUR {props.cryptoValue.toFixed(2)}</Text>
      ) : null}

      {ButtonsSwitch(props)}
    </View>
  );
};
