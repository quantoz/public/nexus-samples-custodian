export const balancesMock = {
  values: {
    currencyBalances: [
      {
        code: "EUR",
        name: "Euro",
        // locked: 0,
        total: 243.09
        // available: 50,
      }
    ],
    cryptoBalances: [
      {
        code: "BTC",
        name: "Bitcoin",
        // locked: 0,
        total: 0.01502
        // available: 50,
      },
      {
        code: "BCH",
        name: "Bitcoin Cash",
        // locked: 0,
        total: 0.07123
        // available: 50,
      },
      {
        code: "ETH",
        name: "Ethereum",
        // locked: 0,
        total: 0.01502
        // available: 50,
      },
      {
        code: "LTC",
        name: "Litecoin",
        // locked: 0,
        total: 2.08281
        // available: 50,
      },
      {
        code: "XRP",
        name: "Ripple",
        // locked: 0,
        total: 43.10092
        // available: 50,
      }
    ]
  }
};

export const pricesMock = [
  {
    message: "Successfully processed your request",
    errors: ["string"],
    values: {
      created: "2020-07-15T12:21:14Z",
      currencyCode: "BTC",
      prices: {
        EUR: {
          buy: 8059.97,
          sell: 8055.71,
          estimatedNetworkSlowFee: 0,
          estimatedNetworkFastFee: 0,
          updated: "2020-07-15T12:21:14Z"
        }
      }
    }
  },
  {
    message: "Successfully processed your request",
    errors: ["string"],
    values: {
      created: "2020-07-15T12:21:14Z",
      currencyCode: "BCH",
      prices: {
        EUR: {
          buy: 194.82,
          sell: 192.09,
          estimatedNetworkSlowFee: 0,
          estimatedNetworkFastFee: 0,
          updated: "2020-07-15T12:21:14Z"
        }
      }
    }
  },
  {
    message: "Successfully processed your request",
    errors: ["string"],
    values: {
      created: "2020-07-15T12:21:14Z",
      currencyCode: "ETH",
      prices: {
        EUR: {
          buy: 197.39,
          sell: 196.22,
          estimatedNetworkSlowFee: 0,
          estimatedNetworkFastFee: 0,
          updated: "2020-07-15T12:21:14Z"
        }
      }
    }
  },
  {
    message: "Successfully processed your request",
    errors: ["string"],
    values: {
      created: "2020-07-15T12:21:14Z",
      currencyCode: "LTC",
      prices: {
        EUR: {
          buy: 36.21,
          sell: 34.65,
          estimatedNetworkSlowFee: 0,
          estimatedNetworkFastFee: 0,
          updated: "2020-07-15T12:21:14Z"
        }
      }
    }
  },
  {
    message: "Successfully processed your request",
    errors: ["string"],
    values: {
      created: "2020-07-15T12:21:14Z",
      currencyCode: "XRP",
      prices: {
        EUR: {
          buy: 0.158,
          sell: 0.134,
          estimatedNetworkSlowFee: 0,
          estimatedNetworkFastFee: 0,
          updated: "2020-07-15T12:21:14Z"
        }
      }
    }
  }
];
