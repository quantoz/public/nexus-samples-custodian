export default {
  message: "Successfully processed your request",
  errors: ["string"],
  values: {
    page: 0,
    total: 0,
    totalPages: 0,
    // filteringParameters: {
    //   property1: "string",
    //   property2: "string",
    // },
    records: [
      {
        destinationCryptoAddress: "string",
        cryptoCode: "BTC",
        currencyCode: "EUR",
        direction: "RECEIVE",
        status: "COMPLETED",
        balanceMutation: {
          crypto: -0.02344567,
          fiat: 5000
        }
      },
      {
        customerCode: "string",
        accountCode: "string",
        transactionCode: "string",
        linkedTransactionCode: "string",
        callbackUrl: "string",
        destinationCryptoAddress: "string",
        cryptoCode: "BTC",
        currencyCode: "EUR",
        paymentMethodCode: "EX_GENERIC_BTC_EUR",
        type: "DEPOSIT",
        direction: "SEND",
        status: "COMPLETED",
        fees: {
          partnerFeeFiat: 2.5,
          bankFeeFiat: 3.5,
          networkFeeFiat: 4.5,
          networkFeeCrypto: 0.00000276
        },
        requestedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        executedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        balanceMutation: {
          crypto: 0.02344567,
          fiat: -5000
        },
        // data: {
        //   property1: "string",
        //   property2: "string",
        // },
        created: "2020-07-15T14:25:24Z"
      },
      {
        customerCode: "string",
        accountCode: "string",
        transactionCode: "string",
        linkedTransactionCode: "string",
        callbackUrl: "string",
        destinationCryptoAddress: "string",
        cryptoCode: "BTC",
        currencyCode: "EUR",
        paymentMethodCode: "EX_GENERIC_BTC_EUR",
        type: "DEPOSIT",
        direction: "SEND",
        status: "COMPLETED",
        fees: {
          partnerFeeFiat: 2.5,
          bankFeeFiat: 3.5,
          networkFeeFiat: 4.5,
          networkFeeCrypto: 0.00000276
        },
        requestedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        executedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        balanceMutation: {
          crypto: 0.02344567,
          fiat: -5000
        },
        // data: {
        //   property1: "string",
        //   property2: "string",
        // },
        created: "2020-07-15T14:25:24Z"
      },
      {
        customerCode: "string",
        accountCode: "string",
        transactionCode: "string",
        linkedTransactionCode: "string",
        callbackUrl: "string",
        destinationCryptoAddress: "string",
        cryptoCode: "BTC",
        currencyCode: "EUR",
        paymentMethodCode: "EX_GENERIC_BTC_EUR",
        type: "DEPOSIT",
        direction: "RECEIVE",
        status: "COMPLETED",
        fees: {
          partnerFeeFiat: 2.5,
          bankFeeFiat: 3.5,
          networkFeeFiat: 4.5,
          networkFeeCrypto: 0.00000276
        },
        requestedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        executedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        balanceMutation: {
          crypto: 0.02344567,
          fiat: -5000
        },
        // data: {
        //   property1: "string",
        //   property2: "string",
        // },
        created: "2020-07-15T14:25:24Z"
      },
      {
        customerCode: "string",
        accountCode: "string",
        transactionCode: "string",
        linkedTransactionCode: "string",
        callbackUrl: "string",
        destinationCryptoAddress: "string",
        cryptoCode: "BTC",
        currencyCode: "EUR",
        paymentMethodCode: "EX_GENERIC_BTC_EUR",
        type: "DEPOSIT",
        direction: "SEND",
        status: "COMPLETED",
        fees: {
          partnerFeeFiat: 2.5,
          bankFeeFiat: 3.5,
          networkFeeFiat: 4.5,
          networkFeeCrypto: 0.00000276
        },
        requestedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        executedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        balanceMutation: {
          crypto: 0.02344567,
          fiat: -5000
        },
        // data: {
        //   property1: "string",
        //   property2: "string",
        // },
        created: "2020-07-15T14:25:24Z"
      },
      {
        customerCode: "string",
        accountCode: "string",
        transactionCode: "string",
        linkedTransactionCode: "string",
        callbackUrl: "string",
        destinationCryptoAddress: "string",
        cryptoCode: "BTC",
        currencyCode: "EUR",
        paymentMethodCode: "EX_GENERIC_BTC_EUR",
        type: "DEPOSIT",
        direction: "RECEIVE",
        status: "COMPLETED",
        fees: {
          partnerFeeFiat: 2.5,
          bankFeeFiat: 3.5,
          networkFeeFiat: 4.5,
          networkFeeCrypto: 0.00000276
        },
        requestedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        executedAmounts: {
          cryptoAmount: 0.02344567,
          fiatValue: 50,
          cryptoPrice: 4500
        },
        balanceMutation: {
          crypto: 0.02344567,
          fiat: -5000
        },
        // data: {
        //   property1: "string",
        //   property2: "string",
        // },
        created: "2020-07-15T14:25:24Z"
      }
    ]
  }
};
