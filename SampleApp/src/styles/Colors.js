const tintColor = "#FAB92C";

export default {
  tintColor,
  tabIconDefault: "#ccc",
  tabIconSelected: tintColor,
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff",
  background: "#383838",

  darkGray: "#323232",
  lightGray: "#807c80",
  white: "#fff"
};
