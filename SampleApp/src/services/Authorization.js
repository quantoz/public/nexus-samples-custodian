// import { authorize, refresh, revoke } from "react-native-app-auth";

import {
  getUserAsync,
  setUserTokenAsync,
  appendUserAsync,
  setUserAsLoggedInAsync,
  getLoggedInUserEmailAsync,
  deleteUserTokenAsync,
  clearUserLoggedInStatusAsync,
  isUserLoggedInAsync,
  setUserItemAsync
} from "_services/Storage";
import { authorize, refresh, revoke, registerUser } from "_services/Server";
import { authConfig } from "Config";
import { Linking } from "react-native";

const verifyAuthData = async authdata => {
  if (authdata.accessToken == null) {
    throw new Error("No accessToken retreived");
  }

  if (authdata.expiresIn == null) {
    throw new Error("No accessToken expiry retreived");
  }

  if (authdata.refreshToken == null) {
    throw new Error("No refreshToken retreived");
  }
};

var refreshPending = false;
const refreshTokenAsync = async user => {
  try {
    if (refreshPending == false) {
      refreshPending = true;
      if (user != null && typeof user.token != "undefined") {
        var authdata = await refresh(authConfig, {
          refreshToken: user.token.refreshToken
        });

        verifyAuthData(authdata);

        // Add ~1 hour expiration time (3000s)
        authdata.tokenExpirationTime = Date.now() + 3000 * 1000;
        await setUserTokenAsync(user, authdata);
      }

      refreshPending = false;
    }
  } catch (e) {
    refreshPending = false;
    signOut();
  }
};

const revokeTokenAsync = async token => {
  const result = await revoke(authConfig, {
    tokenToRevoke: token,
    sendClientId: true
  });
};

export const createAccountAsync = async (email, password) => {
  if (email == null || email == "") {
    throw Error("Email not filled in");
  }

  if (password == null || password == "") {
    throw Error("Password not filled in");
  }

  // does user already exists in the Async Storage?
  var user = await getUserAsync(email);
  if (user != null) {
    throw Error("User with this email already exists");
  }

  // create Nexus customer with created keyPair
  // + account with Config.DEFAULT_TOKEN by default
  var response = await registerUser({
    email,
    password
  });

  await setUserItemAsync(email, password);

  await logInAsync(email, password);
  user = await authorizeAsync();
};

export const logInAsync = async (email, password) => {
  if (email == null || email == "") {
    throw Error("Email not filled in");
  }

  if (password == null || password == "") {
    throw Error("Password not filled in");
  }

  // first log-out possible logged-in user
  if (await isUserLoggedInAsync()) {
    await signOut();
  }

  // does user exists in the Async Storage?
  // only allow logging in if a keyPair exists
  var user = await getUserAsync(email);
  if (user == null) {
    throw Error("Account does not exist on phone. Please sign up first");
  }

  // get token
  var authdata = await authorize(authConfig, { user: email, password });

  verifyAuthData(authdata);

  // Add ~1 hour token expiration time (3000s)
  authdata.tokenExpirationTime = Date.now() + 3000 * 1000;

  // store user in Async Storage
  // !! this is unsafe !!
  // see https://github.com/FormidableLabs/react-native-app-auth/blob/master/docs/token-storage.md
  await appendUserAsync(email, {
    token: authdata
  });
  await setUserAsLoggedInAsync(email);
};

// check authorization token and refresh if needed
export const authorizeAsync = async () => {
  if (!(await isUserLoggedInAsync())) {
    throw new Error("1. User is not logged in");
  } else {
    var email = await getLoggedInUserEmailAsync();
    var user = await getUserAsync(email);

    if (
      typeof user.token == "undefined" ||
      typeof user.token.tokenExpirationTime == "undefined"
    ) {
      throw new Error("2. User is not logged in");
    } else if (user.token.tokenExpirationTime < Date.now()) {
      !refreshPending && (await refreshTokenAsync(user));
    }

    // return logged-in user
    return await getUserAsync(email);
  }
};

export const signOut = async () => {
  try {
    var email = await getLoggedInUserEmailAsync();
    var user = await getUserAsync(email);
    await deleteUserTokenAsync(email);

    await revokeTokenAsync(user.token.refreshToken);
    await revokeTokenAsync(user.token.accessToken);
  } catch (error) {
    // catch here to just continue log-out
  } finally {
    await clearUserLoggedInStatusAsync();
  }
};
