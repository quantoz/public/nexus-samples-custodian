export const eur = require("../assets/images/euro.png");

export const btc = require("../assets/images/btc.png");
export const bch = require("../assets/images/bch.png");
export const eth = require("../assets/images/eth.png");
export const ltc = require("../assets/images/ltc.png");
export const xlm = require("../assets/images/xlm.png");
export const xrp = require("../assets/images/xrp.png");
export const unk = require("../assets/images/unk.png");
